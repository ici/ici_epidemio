#=
SEIR:
- Julia version: 1.8.3
- Author: Nicolas Gilet
- Date: 2023-06-13
=#
export SEIR ;
"""
Function SEIR

Function that define an SEIR epidemiological model (susceptible, infected, recovered)

Inputs:
- alpha: Float64: incubation rate (between 0 and 1)
- beta: Float64: infected rate (between 0 and 1)
- gamma: Float64: recovery rate (between 0 and 1)
- S0: Float64: rate of susceptible individuals at started time
- nb_day: Int64: number of day considered in the modeling
- nb_pop: Int64: total number of individuals

Output :
- I_per_day: Array{Float64}: vector containing the number of new infected individuals per day, expressed per
                             100 000 individuals


other implementation:https://julia.quantecon.org/continuous_time/seir_model.html
"""
function SEIR(α::Float64,β::Float64,γ::Float64,S0::Float64,nb_day::Int64,nb_pop::Int64)

    ###################### Initialisation #########################
    S = zeros(Float64,nb_day+1) ;
    E = zeros(Float64,nb_day+1) ;
    I = zeros(Float64,nb_day+1) ;
    I_per_day = zeros(Float64,nb_day) ;
    R = zeros(Float64,nb_day+1) ;

    ########### Compute the basic reproductive number #############
    R0 = β/γ ;
    println("Computed SEIR model with R_0 = ", R0) ;

    ################### Initial conditions ########################
    S[1] = S0*nb_pop ;
    I[1] = (1.0-S0)*nb_pop ;

    ############ Computation of the states per day #################
    for i=1:nb_day
        S[i+1] = (1.0 - β*I[i]/nb_pop)*S[i] ;
        E[i+1] = β*S[i]*I[i]/nb_pop + (1.0-α)*E[i] ;
        I[i+1] = α*E[i] + (1.0-γ)*I[i] ;
        R[i+1] = γ*I[i]+R[i] ;
        I_per_day[i] = (β*I[i]*S[i]/nb_pop)*(100000.0/nb_pop) ;
    end
# 	println(I_per_day)
	println(sum(I_per_day))
    return(S,E,I,R,sum(I_per_day)) ;

end


export SEIR_NSGA2_MSE ;
"""
Function SEIR_NSGA2_MSE

Function that defined an SEIR model and compute the mean squared error between the incidence computed
by the ICI model and the real incidence

Input :
- x: Array{Float64}: vector containing: (i) alpha: the incubtation rate (between 0 and 1), (ii) beta: the infected rate (between 0 and 1)
                      (iii) gamma: recovery rate (between 0 and 1), (iv) S0: rate of susceptibles individuals at started time
- path - the result of another model to compare. Must be the number of contamination per day with the exact same number of day as the input parameter
Output :
- res: Float64: mean squared error between the incidence computed by the model and the compared incidence
"""
function SEIR_NSGA2_MSE(x::Array{Float64,1},nb_day::Int64,nb_pop::Int64,path::String )

    ############### Import of the real data ########################
    incidence_compared = readdlm(path)[1:nb_day,1]/4. ;
    ################################################################

    #################### Initialisation ############################
    S = zeros(Float64,nb_day+1) ;
    E = zeros(Float64,nb_day+1) ;
    I = zeros(Float64,nb_day+1) ;
    I_per_day = zeros(Float64,nb_day) ;
    R = zeros(Float64,nb_day+1) ;
    ################################################################

    ########### Compute the basic reproductive number #############
    R0 = x[2]/x[3] ;
    #println("Compute SEIR model with R_0 = ", R0) ;
    ################################################################

    ################### Initial conditions #########################
    S[1] = x[4]*nb_pop ;
    I[1] = (1.0-x[4])*nb_pop ;
    ################################################################

    ############ Computation of the states per day #################
    for i=1:nb_day
        S[i+1] = (1.0 - x[2]*I[i]/nb_pop)*S[i] ;
        E[i+1] = x[2]*S[i]*I[i]/nb_pop + (1.0-x[1])*E[i] ;
        I[i+1] = x[1]*E[i] + (1.0-x[3])*I[i] ;
        R[i+1] = x[3]*I[i]+R[i] ;
        I_per_day[i] = (x[2]*I[i]*S[i]/nb_pop)*(100000.0/nb_pop) ;
    end
    ################################################################

    ################## Computation of mse ##########################
    res = 0 ;
    for i=1:nb_day
        res = res + (incidence_compared[i]-I_per_day[i])^2
    end
    ################################################################

    return(res) ;

end



export SEIR1R2_NSGA2
"""
Function SEIR1R2_NSGA2

Function that defined an SEIR1R2 model and compute the mean squared error between the hospital discharges computed
by the ICI model and the real one

Input :
- x: Array{Float64}: vector containing: (i) alpha: the incubtation rate (between 0 and 1), (ii) beta: the infected rate (between 0 and 1)
                      (iii) gamma: recovery rate (between 0 and 1), (iv) S0: rate of susceptibles individuals at started tome

Output :
- res: Float64: mean squared error between the hospital discharges computed by the model and the real incidence
"""
function SEIR1R2_NSGA2(x::Array{Float64,1},nb_day::Int64,nb_pop::Int64,path::String)


    ############### Import of the real data ########################
    dR1_real = readdlm(path)[1:nb_day,1] ;
    ################################################################

    #################### Initialisation ############################
    S = zeros(Float64,nb_day+1) ;
    E = zeros(Float64,nb_day+1) ;
    I = zeros(Float64,nb_day+1) ;
    dR1_calc = zeros(Float64,nb_day+1) ;
    dR2_calc = zeros(Float64,nb_day+1) ;
    ################################################################

    ########### Compute the basic reproductive number #############
    R0 = x[2]/x[3] ;
    #println("Compute SEIR model with R_0 = ", R0) ;
    ################################################################

    ################### Initial conditions #########################
    S[1] = x[4]*nb_pop ;
    I[1] = (1.0-x[4])*nb_pop ;
    ################################################################

    ############ Computation of the states per day #################
    for i=1:nb_day
        S[i+1] = (1.0 - x[2]*I[i]/nb_pop)*S[i] ;
        E[i+1] = x[2]*S[i]*I[i]/nb_pop + (1.0-x[1])*E[i] ;
        I[i+1] = x[1]*E[i] + (1.0-x[3])*I[i] ;
        dR1_calc[i+1] = x[5]*x[3]*I[i] ;
        dR2_calc[i+1] = (1-x[5])*x[3]*I[i] ;
    end
    ################################################################

    ################## Computation of mse ##########################
    res = 0 ;
    for i=1:nb_day
        res = res + (dR1_real[i]-dR1_calc[i])^2
    end
    ################################################################


    return(res) ;

end


export SEIR1R2
"""
Function SEIR1R2

Function that defined an SEIR1R2 model and compute the mean squared error between the hospital discharges computed
by the ICI model and the real one.

Input :
- x: Array{Float64}: vector containing: (i) alpha: the incubation rate (between 0 and 1), (ii) beta: the infected rate (between 0 and 1)
                      (iii) gamma: recovery rate (between 0 and 1), (iv) S0: rate of susceptibles individuals at started tome

Output :
- dR1_calc: Array{Float64,1}: number of hospital discharges computed by the model
- res: Float64: mean squared error between the hospital discharges computed by the model and the real one
"""
function SEIR1R2(x::Array{Float64,1},nb_day::Int64,nb_pop::Int64,path::String)

    ############### Import of the real data ########################
    dR1_real = readdlm(path)[1:nb_day,1] ;
    ################################################################

    #################### Initialisation ############################
    S = zeros(Float64,nb_day+1) ;
    E = zeros(Float64,nb_day+1) ;
    I = zeros(Float64,nb_day+1) ;
    dR1_calc = zeros(Float64,nb_day+1) ;
    dR2_calc = zeros(Float64,nb_day+1) ;
    ################################################################

    ########### Compute the basic reproductive number #############
    R0 = x[2]/x[3] ;
    #println("Compute SEIR model with R_0 = ", R0) ;
    ################################################################

    ################### Initial conditions #########################
    S[1] = x[4]*nb_pop ;
    I[1] = (1.0-x[4])*nb_pop ;
    ################################################################

    ############ Computation of the states per day #################
    for i=1:nb_day
        S[i+1] = (1.0 - x[2]*I[i]/nb_pop)*S[i] ;
        E[i+1] = x[2]*S[i]*I[i]/nb_pop + (1.0-x[1])*E[i] ;
        I[i+1] = x[1]*E[i] + (1.0-x[3])*I[i] ;
        dR1_calc[i+1] = x[5]*x[3]*I[i] ;
        dR2_calc[i+1] = (1-x[5])*x[3]*I[i] ;
    end
    ################################################################

    ################## Computation of mse ##########################
    res = 0 ;
    for i=1:nb_day
        res = res + (dR1_real[i]-dR1_calc[i])^2
    end
    ################################################################

    ##################### Save results #############################
    writedlm("Res_dR1.txt",dR1_calc) ;
    ################################################################

    return(dR1_calc,res) ;

end




