#=
SIR:
- Julia version: 1.10.6
- Author: Nicolas Gilet
- Date: 2023-06-13
=#
using DelimitedFiles
using Evolutionary

export SIR ;
"""
Function SIR

Function that define an SIR epidemiological model (susceptible, infected, recovered)

Inputs:
- beta: Float64: infected rate (between 0 and 1)
- gamma: Float64: recovery rate (between 0 and 1)
- S0: Float64: rate of susceptible individuals at started time
- nb_day: Int64: number of day considered in the modeling

Output :
- S: Array{Float64,1}: vector containing the total number of susceptible population per day
- I: Array{Float64,1}: vector containing the total number of infected population per day
- R: Array{Float64,1}: vector containing the total number of recovered population per day
"""
function SIR(beta::Float64,gamma::Float64,S0::Float64,nb_day::Int64)

    ###################### Initialisation #########################
    S = zeros(Float64,nb_day+1) ; # Susceptible individuals
    I = zeros(Float64,nb_day+1) ; # Infected individuals
    R = zeros(Float64,nb_day+1) ; # Recovered individuals

    ########### Compute the basic reproductive number #############
    R0 = beta/gamma ;
    println("Compute SIR model with R_0 = ", R0) ;

    ################### Initial conditions ########################
    S[1] = S0 ;
    I[1] = 1.0 - S0 ;

    ############ Computation of S, I, R per day ###################
    for i=1:nb_day
        S[i+1] = (1.0 - beta*I[i])*S[i] ;
        I[i+1] = (1.0 + beta*S[i] - gamma)*I[i] ;
        R[i+1] = gamma*I[i]+R[i] ;
    end

    return(S,I,R)

end
